FROM python:3.8-slim-buster

# Install system requirements
RUN apt-get update && \
    apt-get install -y --no-install-recommends build-essential gettext libpq-dev zlib1g-dev libjpeg62-turbo-dev \
        git libmagic-dev && \
    rm -rf /var/lib/apt/lists/*

# Copy Python requirements dir and Install requirements
RUN pip install -U pip setuptools wheel poetry

ARG DPT_VENV_CACHING

ENV POETRY_VIRTUALENVS_CREATE=0

COPY pyproject.toml /
COPY poetry.lock /

RUN poetry install

# Set the default directory where CMD will execute
WORKDIR /app

# Run Django's runserver by default
CMD gunicorn -b 0.0.0.0:80 backend.wsgi:app
