import os

from redis import StrictRedis


def get_redis():
    redis = StrictRedis(
        host=os.environ.get("MEADOWS_REDIS_HOST", "localhost"),
        port=int(os.environ.get("MEADOWS_REDIS_PORT", "6379")),
        db=int(os.environ.get("MEADOWS_REDIS_DB", "0")),
    )
    return redis
