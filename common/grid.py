import os

from common.character import Character, JuicyGrass


class Grid:
    def __init__(self, redis):
        self.width = int(os.environ.get("MEADOWS_GRID_WIDTH", "32"))
        self.height = int(os.environ.get("MEADOWS_GRID_HEIGHT", "24"))
        self.redis = redis
        self.objects = {}

    def update_all_from_redis(self):
        redis_known_keys = set()

        for full_key in self.redis.scan_iter("meadow:item:*"):
            item_key = self.redis.get(full_key).decode()
            try:
                item_type = self.redis.get(f"meadow:type:{item_key}").decode()
            except AttributeError:
                item_type = None
            if item_type == Character.REDIS_TYPE:
                item = Character.from_redis(self.redis, self, item_key)
                if item:
                    self.objects[item_key] = item
                    redis_known_keys.add(item_key)
            elif item_type == JuicyGrass.REDIS_TYPE:
                item = JuicyGrass.from_redis(self.redis, self, item_key)
                if item:
                    self.objects[item_key] = item
                    redis_known_keys.add(item_key)

        for item_key in self.objects.keys() - redis_known_keys:
            del self.objects[item_key]

    def update_from_redis(self, item_key: str):
        if self.redis.get(f"meadow:item:{item_key}") is None:
            if item_key in self.objects:
                del self.objects[item_key]

        else:
            try:
                item_type = self.redis.get(f"meadow:type:{item_key}").decode()
            except AttributeError:
                item_type = None

            if item_type == Character.REDIS_TYPE:
                item = Character.from_redis(self.redis, self, item_key)
                if item:
                    self.objects[item_key] = item
            elif item_type == JuicyGrass.REDIS_TYPE:
                item = JuicyGrass.from_redis(self.redis, self, item_key)
                if item:
                    self.objects[item_key] = item

    def clear(self):
        for full_key in self.redis.scan_iter("meadow:*"):
            self.redis.delete(full_key)
