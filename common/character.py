import dataclasses
import logging
import random
import typing
from datetime import datetime
from uuid import uuid4

from common.names import get_random_name
from common.redis_connection import get_redis

logging.basicConfig(level=logging.INFO)


class CharacterOperationException(Exception):
    pass


class IllegalMove(CharacterOperationException):
    pass


class IllegalMoveBlocked(IllegalMove):
    pass


class IllegalMoveBorder(IllegalMove):
    pass


class IllegalEating(CharacterOperationException):
    pass


class IllegalEatingEmpty(IllegalEating):
    pass


class IllegalEatingAlreadyEaten(IllegalEating):
    pass


@dataclasses.dataclass
class Observation:
    entity_type: int
    entity_name: typing.Optional[str] = None
    extra: typing.Optional[str] = None


class JuicyGrass:
    """todo: this and Character need to have a superclass that implements base logic, currently
    a lot is duplicated"""

    REDIS_TYPE = "grass"

    def __init__(self, item_id, x, y, /, redis=None, grid=None):
        self.x = x
        self.y = y
        self.item_id = item_id
        self.redis = redis if redis else get_redis()
        self.grid = grid
        if self.grid is None:
            from common.grid import Grid  # noqa

            self.grid = Grid(self.redis)

    @classmethod
    def from_redis(cls, redis, grid, item_id) -> typing.Optional["JuicyGrass"]:
        try:
            return cls(
                item_id,
                int(redis.get(f"meadow:x:{item_id}")),
                int(redis.get(f"meadow:y:{item_id}")),
                redis=redis,
                grid=grid,
            )
        except (ValueError, TypeError):
            logging.exception("Couldn't load grass")
            return None

    @classmethod
    def new_from_redis(cls, redis, grid) -> "JuicyGrass":
        item_id = str(uuid4())

        # Book the cell
        reserved_cell = False
        x, y = 0, 0
        while not reserved_cell:
            x = random.randint(0, grid.width - 1)
            y = random.randint(0, grid.height - 1)
            reserved_cell = redis.setnx(f"meadow:grass:{x}:{y}", item_id)

        grass = cls(item_id, x, y, redis=redis, grid=grid)
        grass.to_redis()

        return grass

    def to_redis(self):
        self.redis.set(f"meadow:x:{self.item_id}", self.x)
        self.redis.set(f"meadow:y:{self.item_id}", self.y)
        self.redis.set(f"meadow:item:{self.item_id}", self.item_id)
        self.redis.set(f"meadow:type:{self.item_id}", self.REDIS_TYPE)
        self.redis.set(f"meadow:grass:{self.x}:{self.y}", self.item_id)

        self.redis.publish("meadow", self.item_id)

    def remove(self):
        self.redis.delete(f"meadow:x:{self.item_id}")
        self.redis.delete(f"meadow:y:{self.item_id}")
        self.redis.delete(f"meadow:item:{self.item_id}")
        self.redis.delete(f"meadow:type:{self.item_id}")
        self.redis.delete(f"meadow:grass:{self.x}:{self.y}")

        self.redis.publish("meadow", self.item_id)


class Character:
    REDIS_TYPE = "character"

    DIRECTIONS = {
        0: "North",
        1: "East",
        2: "South",
        3: "West",
    }

    DIRECTION_OFFSETS = {
        0: (0, 1),
        1: (1, 0),
        2: (0, -1),
        3: (-1, 0),
    }

    OBSERVABLE_UNKNOWN = -1
    OBSERVABLE_GRASS = 0
    OBSERVABLE_WATER = 1
    OBSERVABLE_HORSE = 2
    OBSERVABLE_EDIBLE_GRASS = 3
    OBSERVABLES = {
        OBSERVABLE_GRASS: "Short grass",
        OBSERVABLE_WATER: "Water",
        OBSERVABLE_HORSE: "Horse",
        OBSERVABLE_EDIBLE_GRASS: "Long juicy grass",
        OBSERVABLE_UNKNOWN: "Something big and scary",
    }

    def __init__(
        self, character_id, x, y, direction, fullness, name, /, redis=None, grid=None
    ):
        self.x = x
        self.y = y
        self.character_id = character_id
        self.direction = direction
        self.redis = redis if redis else get_redis()
        self.grid = grid
        self.fullness = fullness
        self.name = name
        if self.grid is None:
            from common.grid import Grid  # noqa

            self.grid = Grid(self.redis)

    @property
    def direction_readable(self):
        return self.DIRECTIONS.get(self.direction, "Unknown")

    @classmethod
    def from_redis(cls, redis, grid, character_id) -> typing.Optional["Character"]:
        try:
            return cls(
                character_id,
                int(redis.get(f"meadow:x:{character_id}")),
                int(redis.get(f"meadow:y:{character_id}")),
                int(redis.get(f"meadow:direction:{character_id}")),
                int(redis.get(f"meadow:fullness:{character_id}")),
                redis.get(f"meadow:name:{character_id}").decode(),
                redis=redis,
                grid=grid,
            )
        except (ValueError, TypeError, AttributeError):
            logging.exception("Couldn't load character")
            return None

    @classmethod
    def new_from_redis(cls, redis, grid) -> "Character":
        character_id = str(uuid4())

        # Book the cell
        reserved_cell = False
        x, y = 0, 0
        while not reserved_cell:
            x = random.randint(0, grid.width - 1)
            y = random.randint(0, grid.height - 1)
            reserved_cell = redis.setnx(f"meadow:cell:{x}:{y}", character_id)

        direction = random.randint(0, 3)

        character = cls(
            character_id, x, y, direction, 0, get_random_name(), redis=redis, grid=grid
        )
        character.to_redis()

        return character

    def to_redis(self):
        self.redis.set(f"meadow:x:{self.character_id}", self.x)
        self.redis.set(f"meadow:y:{self.character_id}", self.y)
        self.redis.set(f"meadow:item:{self.character_id}", self.character_id)
        self.redis.set(f"meadow:type:{self.character_id}", self.REDIS_TYPE)
        self.redis.set(f"meadow:fullness:{self.character_id}", self.fullness)
        self.redis.set(f"meadow:name:{self.character_id}", self.name)
        self.redis.set(f"meadow:direction:{self.character_id}", self.direction)
        self.redis.set(f"meadow:cell:{self.x}:{self.y}", self.character_id)
        self.redis.set(
            f"meadow:timestamp:{self.character_id}", datetime.now().timestamp()
        )

        self.redis.publish("meadow", self.character_id)

    def remove(self):
        self.redis.delete(f"meadow:x:{self.character_id}")
        self.redis.delete(f"meadow:y:{self.character_id}")
        self.redis.delete(f"meadow:item:{self.character_id}")
        self.redis.delete(f"meadow:type:{self.character_id}")
        self.redis.delete(f"meadow:fullness:{self.character_id}")
        self.redis.delete(f"meadow:name:{self.character_id}")
        self.redis.delete(f"meadow:direction:{self.character_id}")
        self.redis.delete(f"meadow:cell:{self.x}:{self.y}")

        self.redis.publish("meadow", self.character_id)

    @property
    def last_update(self):
        return self.redis.get(f"meadow:timestamp:{self.character_id}")

    def move(self, speed=1):
        dx, dy = self.DIRECTION_OFFSETS.get(self.direction, (0, 0))
        nx, ny = self.x + dx * speed, self.y + dy * speed

        if not (0 <= nx < self.grid.width and 0 <= ny < self.grid.height):
            raise IllegalMoveBorder()

        moving = self.redis.setnx(f"meadow:cell:{nx}:{ny}", self.character_id)

        if not moving:
            raise IllegalMoveBlocked()

        # Old coordinates
        ox, oy = self.x, self.y

        # Perform move
        self.x = nx
        self.y = ny
        self.fullness = max(0, self.fullness - 3)

        self.to_redis()

        # Stomp the grass
        try:
            grass_id = self.redis.get(f"meadow:grass:{self.x}:{self.y}").decode()
        except AttributeError:
            grass_id = None
        if grass_id:
            JuicyGrass.from_redis(self.redis, self.grid, grass_id).remove()

        # Old cell is now free
        self.redis.delete(f"meadow:cell:{ox}:{oy}")

    def move_forward(self):
        self.move(speed=1)

    def move_backward(self):
        self.move(speed=-1)

    def turn(self, turns=1):
        self.direction = (self.direction + turns) % 4
        self.to_redis()

    def turn_left(self):
        self.turn(turns=-1)

    def turn_right(self):
        self.turn(turns=1)

    def look_nearby(self, direction) -> Observation:
        dx, dy = self.DIRECTION_OFFSETS.get(direction, (0, 0))
        nx, ny = self.x + dx, self.y + dy
        if not (0 <= nx < self.grid.width and 0 <= ny < self.grid.height):
            return Observation(entity_type=self.OBSERVABLE_WATER, entity_name=None)

        try:
            entity = self.redis.get(f"meadow:cell:{nx}:{ny}").decode()
        except AttributeError:
            try:
                entity = self.redis.get(f"meadow:grass:{nx}:{ny}").decode()
            except AttributeError:
                entity = None
        try:
            entity_type = self.redis.get(f"meadow:type:{entity}").decode()
        except AttributeError:
            entity_type = None

        if entity_type is None:
            return Observation(entity_type=self.OBSERVABLE_GRASS)
        if entity_type == self.REDIS_TYPE:
            try:
                name = self.redis.get(f"meadow:name:{entity}").decode()
            except AttributeError:
                name = entity
            return Observation(entity_type=self.OBSERVABLE_HORSE, entity_name=name)
        if entity_type == JuicyGrass.REDIS_TYPE:
            return Observation(entity_type=self.OBSERVABLE_EDIBLE_GRASS)

        return Observation(entity_type=self.OBSERVABLE_UNKNOWN)

    def look_forward(self):
        return self.look_nearby(self.direction)

    def look_left(self):
        return self.look_nearby((self.direction - 1) % 4)

    def look_right(self):
        return self.look_nearby((self.direction + 1) % 4)

    def look_back(self):
        return self.look_nearby((self.direction + 2) % 4)

    def eat(self):
        dx, dy = self.DIRECTION_OFFSETS.get(self.direction, (0, 0))
        nx, ny = self.x + dx, self.y + dy

        try:
            grass_id = self.redis.get(f"meadow:grass:{nx}:{ny}").decode()
        except AttributeError:
            grass_id = None

        if grass_id:
            grass = JuicyGrass.from_redis(self.redis, self.grid, grass_id)
        else:
            grass = None

        if grass is None:
            raise IllegalEatingEmpty()
        #
        # eaten = self.redis.delete(f"meadow:grass:{nx}:{ny}")
        # if eaten == 0:
        #     raise IllegalEatingAlreadyEaten()

        self.fullness += 10
        grass.remove()
        self.to_redis()
