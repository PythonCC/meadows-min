import pathlib
from datetime import datetime

import pyglet

from common.character import Character, JuicyGrass
from common.grid import Grid
from common.redis_connection import get_redis


class Meadow:
    horse_image_north = pyglet.image.load(
        pathlib.Path(__file__).parent / "images" / "horse_north.png"
    )
    horse_image_south = pyglet.image.load(
        pathlib.Path(__file__).parent / "images" / "horse_south.png"
    )
    horse_image_east = pyglet.image.load(
        pathlib.Path(__file__).parent / "images" / "horse_east.png"
    )
    horse_image_west = pyglet.image.load(
        pathlib.Path(__file__).parent / "images" / "horse_west.png"
    )

    grass_image = pyglet.image.load(
        pathlib.Path(__file__).parent / "images" / "grass.png"
    )

    horse_images = {
        0: horse_image_north,
        1: horse_image_east,
        2: horse_image_south,
        3: horse_image_west,
    }

    for image in [
        horse_image_west,
        horse_image_east,
        horse_image_south,
        horse_image_north,
        grass_image,
    ]:
        image.anchor_x = image.width // 2
        image.anchor_y = image.height // 2

    cell_width = horse_image_north.width
    cell_height = horse_image_north.height

    def __init__(self):
        self.redis = get_redis()
        self.grid = Grid(self.redis)
        self.subscription = self.redis.pubsub()
        self.subscription.subscribe("meadow")

        self.horse_batch = pyglet.graphics.Batch()
        self.grass_batch = pyglet.graphics.Batch()
        self.sprites = {}
        self.leader = None

    # This and the following functions are scheduled with pyglet scheduler,
    # and we don't really care for any arguments it passes in, so allow
    # any arguments
    # pylint: disable=unused-argument
    def check_subscriptions(self, *args, **kwargs):
        message = self.subscription.get_message()
        had_messages = False
        while message is not None:
            if message["type"] == "message":
                item_id = message["data"].decode()
                self.grid.update_from_redis(item_id)
                had_messages = True
            message = self.subscription.get_message()

        if had_messages:
            self.recreate_sprites()
            self.update_leader()

    # pylint: disable=unused-argument
    def grow_grass(self, *args, **kwargs):
        existing_grass = [
            item for item in self.grid.objects.values() if isinstance(item, JuicyGrass)
        ]
        if len(existing_grass) > 0.5 * self.grid.width * self.grid.height:
            # Too much grass
            return

        JuicyGrass.new_from_redis(self.redis, self.grid)

    # pylint: disable=unused-argument
    def remove_stale_horses(self, *args, **kwargs):
        horses = [
            item for item in self.grid.objects.values() if isinstance(item, Character)
        ]

        for horse in horses:
            last_update_time = horse.last_update
            if (
                last_update_time is None
                or datetime.now().timestamp() - float(last_update_time) > 300
            ) and horse.fullness == 0:
                horse.remove()

    def recreate_sprites(self):
        for item_key, item in self.grid.objects.items():
            if isinstance(item, Character):
                image = self.horse_images.get(item.direction, self.horse_image_east)
                batch = self.horse_batch
            elif isinstance(item, JuicyGrass):
                image = self.grass_image
                batch = self.grass_batch
            else:
                image = None
                batch = None

            if image and batch:
                if item_key in self.sprites:
                    self.sprites[item_key].x = (
                        item.x * self.cell_width + self.cell_width
                    )
                    self.sprites[item_key].y = (
                        item.y * self.cell_height + self.cell_height
                    )
                    self.sprites[item_key].image = image
                else:
                    self.sprites[item_key] = pyglet.sprite.Sprite(
                        image,
                        item.x * self.cell_width + self.cell_width,
                        item.y * self.cell_height + self.cell_height,
                        batch=batch,
                    )

        for item_key in set(self.sprites.keys()) - set(self.grid.objects.keys()):
            self.sprites[item_key].x = -self.cell_width
            self.sprites[item_key].y = -self.cell_height
            del self.sprites[item_key]

    def update_leader(self):
        if self.leader:
            horses = [
                item
                for item in self.grid.objects.values()
                if isinstance(item, Character)
            ]
            horses.sort(key=lambda item: -item.fullness)
            if not horses:
                self.leader.text = "No leader yet"
            else:
                self.leader.text = f"{horses[0].name} is least hungry"

    def run(self):
        self.grid.update_all_from_redis()
        self.recreate_sprites()

        header_offset = 32

        window = pyglet.window.Window(
            width=(self.grid.width + 1) * self.cell_width,
            height=(self.grid.height + 1) * self.cell_height + header_offset,
        )
        water = pyglet.image.SolidColorImagePattern((200, 200, 255, 255)).create_image(
            window.width, window.height
        )
        grass = pyglet.image.SolidColorImagePattern((200, 255, 200, 255)).create_image(
            window.width - self.cell_width,
            window.height - self.cell_height - header_offset,
        )

        self.leader = pyglet.text.Label(
            "No leader yet",
            font_size=header_offset - 6,
            x=window.width // 2,
            y=window.height - header_offset + 6,
            anchor_x="center",
            anchor_y="center",
        )

        self.update_leader()

        @window.event
        def on_draw():
            # clear the window
            window.clear()

            # add background
            water.blit(0, 0)
            grass.blit(self.cell_width // 2, self.cell_height // 2)

            # draw the batches
            self.grass_batch.draw()
            self.horse_batch.draw()

            self.leader.draw()

        pyglet.clock.schedule(self.check_subscriptions, 0.1)
        pyglet.clock.schedule(self.grow_grass, 10)
        pyglet.clock.schedule(self.remove_stale_horses, 60)

        pyglet.app.run()
